#!/usr/bin/python

import os

kudoers = {}
var = []
activities = {}

def parse_activity(kudos_raw,var):
	users = []
	for x in kudos_raw.replace('[{','').replace('}]','').split('},{'):
		user = []
		for y in x.split(','):
			user.append(y.split('":')[1].replace('"',''))
		users.append(user)
	
	
	a_id = ( x[0] for x in users )
	resource_state = ( x[1] for x in users )
	firstname = ( x[2] for x in users )
	lastname = ( x[3] for x in users )
	profile_medium = ( x[4] for x in users )
	profile = ( x[5] for x in users )
	city = ( x[6] for x in users )
	state = ( x[7] for x in users )
	country = ( x[8] for x in users )
	sex = ( x[9] for x in users )
	friend = ( x[10] for x in users )
	follower = ( x[11] for x in users )
	premium = ( x[12] for x in users )
	created_at = ( x[13] for x in users )
	updated_at = ( x[14] for x in users )
	var.append(list(a_id))
	var.append(list(resource_state))
	var.append(list(firstname))
	var.append(list(lastname))
	var.append(list(profile_medium))
	var.append(list(profile))
	var.append(list(city))
	var.append(list(state))
	var.append(list(country))
	var.append(list(sex))
	var.append(list(friend))
	var.append(list(follower))
	var.append(list(premium))
	var.append(list(created_at))
	var.append(list(updated_at))

def add_kudoers():
	ids = var[0]
	
	i = 0 #user nbr
	while i < len(ids):
		tmp = []
		j = 1 #list nbr
		while j < len(var):
			if j in {2,3,4,6,7,8,9,10,11,12,13}:
				tmp.append(var[j][i])
			j = j+1
		tmp.append(1) #update kudos count

		if ids[i] in kudoers:
			#update count
			kudoers[ids[i]][11]+=1
		else:
			kudoers[ids[i]] = tmp
		i = i+1
	del var[:]
	
def get_kudoers():
	for keys,values in kudoers.items():
		print(keys)
		print(values)
		print('------------------------')
	
def process_kudos_file(activity_id):
	kudos_file = open('/home/user/8624080978997ecdb2781ed9c511a914abafdf7a/Downloads/activity_history/' + activity_id + '_kudos.txt','r')
	kudos_raw = kudos_file.read()
	if kudos_raw != '[]':
		parse_activity(kudos_raw,var)
		add_kudoers()
	else:
		print('*** ' + activity_id)


def create_html():
	f = open('kudoers.html','w')
	open_html = '<!DOCTYPE html>\n<html>\n<body>\n'
	f.write(open_html)

	create_gender_breakdown(f)
	create_table_of_kudoers(f)

	close_html = '</body>\n</html>'
	f.write(close_html)
	f.close()

def create_table_of_kudoers(f):
	create_table = '<table style="width:100%">\n'
	f.write(create_table)
	for k in kudoers:
		open_athlete = '\n<tr>\n'
		ath_pic = '<td><img src="' + kudoers[k][2] + '"></td>'
		ath_name = '<td><a href="http://www.strava.com/athletes/' + k + '">' + kudoers[k][0] + ' ' + kudoers[k][1] + '</a></td>\n'
		kudos_ct = '<td>' + str(kudoers[k][11]) + '</td>'

		close_athlete = '\n</tr>\n'
		
		f.write(open_athlete)
		f.write(ath_pic)
		f.write(ath_name)
		f.write(kudos_ct)
		f.write(close_athlete)
	close_table = '\n</table>'
	f.write(close_table)

def create_gender_breakdown(f):
	w = 0
	m = 0
	u = 0
	for k in kudoers:
		if kudoers[k][6] == 'F':
			w += 1
		elif kudoers[k][6] == 'M':
			m += 1
		else:
			u += 1
	
	create_table = '<table style="width:100%">\n'
	f.write(create_table)
	f_str = '<tr><td>Female</td><td>' + str(w) + '</td></tr>'
	m_str = '<tr><td>Male</td><td>' + str(m) + '</td></tr>'
	u_str = '<tr><td>Unknown</td><td>' + str(u) + '</td></tr>'
		
	f.write(f_str)
	f.write(m_str)
	f.write(u_str)
	close_table = '\n</table>'
	f.write(close_table)




def parse_athlete_activities(activities_raw):
	#activities = []
	for x in activities_raw.replace('[{',',{').replace('}]','').split('},{'):
		activity_id = -1
		activity = []
		i = 0
		for y in x.split(',"'):
			if i == 0:
				activity_id = y.replace('"','').split(':')[1]
			if i in {6,7,8,9,10,11,13,17,18,19,23,24,25,26,30,31,35,36,38,41,42,43,44,45}: # 6:name, 7:distance, 8:moving time, 9:elapsed time, 10:elevation gain, 11:activity type, 13:local start date, 17:city, 18:state, 19:country, 23:kudos count, 24:comment count, 25:athlete count, 26:photo count, 30:trainer, 31:commute, 35:gear id, 36:avg speed, 38:avg cadence, 41:weighted avg watts, 42:kj, 43:device watts, 44:avg hr, 45:max hr
				#activity.append(y.split('":')[1].replace('"',''))
				activity.append(y.replace('"',''))
			i += 1
		activities[activity_id] = activity


def process_athlete_activities_file():
	activities_file = open(DIRECTORY + athlete_id + '_activities.txt','r')
	activities_raw = activities_file.read()
	parse_athlete_activities(activities_raw)
	#add_activities()

auth_key = AUTH_KEY
def main():
	process_athlete_activities_file()
	# curl activities for kudos
	for activity in activities:
		print(activity)
		cmd_str = 'curl -G https://www.strava.com/api/v3/activities/' + activity + '/kudos -H "Authorization: Bearer ' + auth_key + '" > ' + activity + '_kudos.txt'
		os.system(cmd_str)
		process_kudos_file(activity)
		print('================================================================')
	get_kudoers()
	create_html()

main()
